int closedButton = 2;
int openedButton = 3;
int upRelay = 8;
int downRelay = 9;
int lightRelay = 10;
int moveButton = 4;

int lastState = 111;

//cero -> Stop
//uno -> Up move
//dos -> Down move
int moveStatus = 0;
int nextMove = 0;
unsigned long moveStartedTime = 0;
unsigned long lightStartedTime = 0;

const unsigned long MAX_ELAPSEDTIME = 1000 * 15;
const unsigned long MAX_ELAPSEDSTARTTIME = 1000 * 4;

const unsigned long MAX_LIGHT_ON = 1000 * 60 * 3;

void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);

  // make the pushbutton's pin an input:
  pinMode(closedButton, INPUT);
  pinMode(openedButton, INPUT);
  pinMode(moveButton, INPUT);
  pinMode(upRelay, OUTPUT);
  pinMode(downRelay, OUTPUT);
  pinMode(lightRelay, OUTPUT);

}

void calculateNextMove() {
  if (moveStatus == 1) {
    nextMove = 2;
  } 
  else {
    nextMove = 1;
  }
}

bool isOverrunning(int closedButtonState, int openedButtonState) {
  if(moveStatus != 0){

    unsigned long elapsed = millis() - moveStartedTime;
    //El segundo caso es cuando el moton se quedó trabado en la salida.
    return (elapsed > MAX_ELAPSEDTIME || ((closedButtonState == 1 || openedButtonState == 1) && elapsed > MAX_ELAPSEDSTARTTIME));
  }
  else{
    return false;
  }
}

void initStartedTime() {
  moveStartedTime = millis();
}

void stopDownMove() {

  if (moveStatus == 2) {
    Serial.println("Cerrado.");
    calculateNextMove();
    moveStatus = 0;
    moveStartedTime = 0;
  }
  digitalWrite(downRelay, LOW);

}

void stopUpMove() {

  if (moveStatus == 1) {
    Serial.println("Abierto.");
    calculateNextMove();
    moveStatus = 0;
    moveStartedTime = 0;

  }
  digitalWrite(upRelay, LOW);

}

void stopNowMove() {

  if (moveStatus == 1 || moveStatus == 2) {
    Serial.println("En la mitad.");
    calculateNextMove();
    moveStatus = 0;
    moveStartedTime = 0;

  }
  digitalWrite(upRelay, LOW);

}

void startDownMove() {

  if (moveStatus == 0) {
    Serial.println("Cerrando ...");
    moveStatus = 2;
    initStartedTime();
  }
  digitalWrite(upRelay, LOW);
  digitalWrite(downRelay, HIGH);

}

void turnOnLight() {
  if(lightStartedTime == 0 ){
    Serial.println("Luces prendidas.");
    digitalWrite(lightRelay, HIGH);
  }
  lightStartedTime = millis();

}

void turnOffLight() {
  Serial.println("Luces apagadas.");
  digitalWrite(lightRelay, LOW);
  lightStartedTime = 0;

}

void startUpMove() {
  if (moveStatus == 0) {
    Serial.println("Abriendo ...");
    moveStatus = 1;
    initStartedTime();

  }
  digitalWrite(downRelay, LOW);
  digitalWrite(upRelay, HIGH);

}

void startNowMove() {
  if (nextMove == 1) {
    startUpMove();
  } 
  else {
    startDownMove();
  }

}

void logInputs(int closedButtonState, int openedButtonState, int moveButtonState) {
  Serial.println("closedButtonState: " + String(closedButtonState));
  Serial.println("openedButtonState: " + String(openedButtonState));
  Serial.println("moveButtonState: " + String(moveButtonState));
  Serial.println("#########################################");
}

void controlLight(int newState) {
  //Serial.println(String(newState));
  if (newState != lastState) {
    turnOnLight();
    lastState = newState;
  }
  if(lightStartedTime != 0){
    unsigned long elapsed = millis() - lightStartedTime;
    //Serial.println("Elapsed de luces:" + String(elapsed));
    //Serial.println("MAX:" + String(MAX_LIGHT_ON));

    if (elapsed > MAX_LIGHT_ON) {
      turnOffLight();
      lightStartedTime = 0;
    }

  }

}

void loop() {

  int closedButtonState = digitalRead(closedButton);
  int openedButtonState = digitalRead(openedButton);
  int moveButtonState = digitalRead(moveButton);

  int newState = closedButtonState +1 + ((openedButtonState+1) * 10) + ((moveButtonState+1) * 100);
  controlLight(newState);

  //logInputs(closedButtonState, openedButtonState, moveButtonState);

  if (closedButtonState == 1 && moveButtonState == 0) {
    stopDownMove();
  } 
  else if (closedButtonState == 1 && moveButtonState == 1 && moveStatus == 0) {
    startUpMove();
  } 
  else if (openedButtonState == 1 && moveButtonState == 0) {
    stopUpMove();
  } 
  else if (openedButtonState == 1 && moveButtonState == 1 && moveStatus == 0) {
    startDownMove();
  } 
  else if ((closedButtonState == 0) && (openedButtonState == 0) && (moveButtonState == 1) && (moveStatus != 0)) {
    stopNowMove();
  } 
  else if ((closedButtonState == 0) && (openedButtonState == 0) && (moveButtonState == 1) && (moveStatus == 0)) {
    startNowMove();
  } 
  if (isOverrunning(closedButtonState, openedButtonState)) {
    stopNowMove();
    Serial.println("Frenado forzoso.");

  }

  delay(30);
}



